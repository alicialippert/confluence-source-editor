# Confluence Source Editor Plugin

The Confluence Source Editor plugin allows users to view and edit the underlying storage format for a Confluence page. The source editor is ideal for:

* Copy/pasting back and forth between Confluence and your preferred desktop editor
* Quickly viewing and editing macro parameters, link URLs, image names and more.
* Quickly fixing editor formatting errors
* Search and replace macro parameters and link urls.

When the plugin is installed and enabled on your Confluence site, users will see the new source editor button <> in the editor toolbar. Features include:

* XML syntax highlighting and line numbers to make the source easy to read.
* Simple validation ensures that your XHTML is well formed before you apply the changes to the page.

Access to the Source Editor may be restricted to specific groups by the system administrator.

Documentation for the Confluence Storage Format is available [here](http://confluence.atlassian.com/display/DOC/Confluence+Storage+Format).

This plugin is available on the [Atlassian Marketplace](https://marketplace.atlassian.com/plugins/com.atlassian.confluence.plugins.editor.confluence-source-editor).